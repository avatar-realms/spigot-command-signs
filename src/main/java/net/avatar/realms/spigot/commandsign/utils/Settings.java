package net.avatar.realms.spigot.commandsign.utils;

public abstract class Settings {

	public final static char opChar = '^';

	public final static char serverChar = '#';

	public final static boolean showPluginMessages = true;

}
