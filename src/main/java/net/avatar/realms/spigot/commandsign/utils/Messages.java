package net.avatar.realms.spigot.commandsign.utils;

public class Messages {

	public static String NO_PERMISSION = "You do NOT have the permission to use this command.";

	public static String PLAYER_COMMAND = "You must be a player to execute this command.";

	public static String COMMAND_NEEDS_ARGUMENTS = "You must specify at least one argument to use this command.";

	public static String ALREADY_CREATING_CONFIGURATION = "You are already creating a configuration.";

	public static String ALREADY_EDITING_CONFIGURATION = "You are already editing a configuration.";

	public static String ALREADY_DELETING_CONFIGURATION = "You are already deleting a configuration.";

	public static String ALREADY_INFO_CONFIGURATION = "You are already getting information about a configuration.";

	public static String ALREADY_COPYING_CONFIGURATION = "You are already copying a configuration.";

	public static String COMMAND_NEEDS_RADIUS = "You have to specify the radius to use this command.";

	public static String NUMBER_ARGUMENT = "The argument must be a number.";

	public static String INVALID_COMMAND_ID = "No command block was found with for this ID.";

	public static String NO_NAME = "No Name";


}
