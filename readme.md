Description :  
This plugin offers to admins the possibility to create Command signs, button or plates.
When a player interact with these blocks, they will run the command on the block.

Admins also have the possibility to provide temporary permissions to players.
E.g : if your players do not have the warp permission but you want them to be able to /warp arena by a command sign, you could add the temporary permissions :
essentials.warp
essentials.warps.arena

You can also set needed permission for the command block to be usable.
E.g : If you want only VIP people to go into a specific area, you could add the needed permission
server.permissions.vip


Version compatibilities
Command Sign 1.0.2 => Spigot 1.8.7+

